# coffee-chat

Just a little chat application that I created for fun over a weekend. It consists of two microservices and a redis server.

## How to run?

Create a docker-compose.yml file with the following content

```yaml
version: '3.8'
services:
  redis:
    image: redis:7.0.4
  websocket:
    image: registry.gitlab.com/earths_last_hope/coffee-chat/ws
    environment:
      - REDIS_SERVER=redis://redis:6379
    ports:
      - 8888:8888
  chat_webapp:
    image: registry.gitlab.com/earths_last_hope/coffee-chat/web
    ports:
      - 1337:80
```

and then run ```docker compose up```.

Go to http://localhost:1337 in your web browser and start chatting. Try opening a different browser also and visit the website there, see that the chats are working. You can also visit the site from another device on your network by visiting the IP address / host name directly (e.g. http://192.168.0.15:1337).
