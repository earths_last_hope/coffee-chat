import { StateType, ActionType } from 'typesafe-actions';
import { Store } from 'redux';
import rootReducer from './root';

export type RootReducerState = StateType<typeof rootReducer>;
export type RootReducerAction = ActionType<typeof rootReducer>;
export type RootReducerStore = Store<RootReducerState, RootReducerAction>;

export { rootReducer };
