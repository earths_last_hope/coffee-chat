import { combineReducers } from 'redux';
import chat from './chat';
import events from './events';

const rootReducer = combineReducers({
  chat,
  events,
});

export default rootReducer;
