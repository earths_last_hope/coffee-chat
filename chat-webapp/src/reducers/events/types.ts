import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type Event = {
  event: 'user_online' | 'user_offline' | 'all_users_online';
  nickname?: string;
  nicknames?: Array<string>;
  timestamp: number;
};

export type EventState = Event[];
export type EventAction = ActionType<typeof actions>;
