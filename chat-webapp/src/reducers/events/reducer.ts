import { createReducer } from 'typesafe-actions';
import { EventState, EventAction } from './types';
import * as actions from './actions';

const reducer = createReducer<EventState, EventAction>([]).handleAction(
  actions.receiveEvent,
  (state, action) => {
    return [
      {
        ...action.payload.event,
        timestamp: Date.now(),
      },
      ...state,
    ];
  }
);

export default reducer;
