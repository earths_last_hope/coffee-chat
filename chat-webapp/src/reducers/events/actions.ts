import { createAction } from 'typesafe-actions';
import { Event } from './types';

const receiveEvent = createAction('RECEIVE_EVENT', (ev: Event) => ({
  event: ev,
}))();

export { receiveEvent };
