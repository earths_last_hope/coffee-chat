import { createSelector } from 'reselect';
import { ChatReducerState } from './types';

const getIndexFromProps = (state: ChatReducerState, props: { index: number }) =>
  props.index;

const getMessages = (state: ChatReducerState) => state.messages;
const getNickname = (state: ChatReducerState) => state.nickname;
const getNeedsToChangeNickname = (state: ChatReducerState) =>
  state.needsChangeNickname;
const getHistory = (state: ChatReducerState) => state.history;
const getHistoryItemForIndex = createSelector(
  [getHistory, getIndexFromProps],
  (history, index) => {
    if (index < 0 || history.length <= index) {
      return '';
    }

    return history[index];
  }
);

export {
  getMessages,
  getNickname,
  getNeedsToChangeNickname,
  getHistoryItemForIndex,
};
