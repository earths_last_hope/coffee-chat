import { createAction } from 'typesafe-actions';

const receiveChatMessage = createAction(
  'RECEIVE_CHAT_MESSAGE',
  (message: string, sender: string) => ({
    message,
    sender,
  })
)();

const sendChatMessage = createAction(
  'SEND_CHAT_MESSAGE',
  (message: string) => message
)();

const changeNickname = createAction(
  'CHANGE_NICKNAME',
  (nickname: string) => nickname
)();

export { sendChatMessage, receiveChatMessage, changeNickname };
