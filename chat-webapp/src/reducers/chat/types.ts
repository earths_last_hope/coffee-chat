import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type ChatMessage = {
  text: string;
  sender: string;
  timestamp: Number;
};

export type ChatReducerState = {
  messages: Array<ChatMessage>;
  history: Array<string>;
  nickname: string;
  needsChangeNickname: boolean;
};

export type ChatReducerAction = ActionType<typeof actions>;
