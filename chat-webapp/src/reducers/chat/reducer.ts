import { createReducer } from 'typesafe-actions';
import * as actions from './actions';
import * as selectors from './selectors';
import { ChatReducerState, ChatReducerAction, ChatMessage } from './types';

const chatReducer = createReducer<ChatReducerState, ChatReducerAction>({
  nickname: 'Anonymous',
  messages: [],
  history: [],
  needsChangeNickname: true,
})
  .handleAction(actions.receiveChatMessage, (state, action) => {
    const newMessage: ChatMessage = {
      sender: action.payload.sender,
      text: action.payload.message,
      timestamp: Date.now(),
    };
    return {
      ...state,
      messages: [newMessage, ...state.messages],
    };
  })
  .handleAction(actions.sendChatMessage, (state, action) => {
    const lastHistoryItem = selectors.getHistoryItemForIndex(state, {
      index: 0,
    });

    if (action.payload === lastHistoryItem || action.payload === '') {
      return state;
    }

    return {
      ...state,
      history: [action.payload, ...state.history],
    };
  })
  .handleAction(actions.changeNickname, (state, action) => ({
    ...state,
    needsChangeNickname: false,
    nickname: action.payload,
  }));

export default chatReducer;
