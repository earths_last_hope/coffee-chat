import React, { FunctionComponent } from 'react';
import { ChatMessage } from '../../reducers/chat/types';

const MessageFeedEntry: FunctionComponent<ChatMessage> = (props) => {
  const { sender, text } = props;

  return (
    <div className="message-feed-entry">
      <span className="author">{sender}</span>
      <span className="message">{text}</span>
    </div>
  );
};

export default MessageFeedEntry;
