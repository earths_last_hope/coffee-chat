import React from 'react';
import { useSelector } from 'react-redux';
import { getMessages } from '../../reducers/chat/selectors';
import { RootReducerState } from '../../reducers';
import MessageFeedEntry from './MessageFeedEntry';
import EventEntry from './EventEntry';
import { createSelector } from 'reselect';
import { ChatMessage } from '../../reducers/chat/types';
import { Event } from '../../reducers/events/types';

const rootSelectMessages = (state: RootReducerState) => getMessages(state.chat);
const getEvents = (state: RootReducerState) => state.events;

const getFeed = createSelector(
  [rootSelectMessages, getEvents],
  (messages, events) => {
    return [
      ...messages.map((m) => ({ type: 'message', ...m })),
      ...events.map((m) => ({ type: 'event', ...m })),
    ].sort((a, b) => {
      if (a.timestamp < b.timestamp) {
        return 1;
      } else if (a.timestamp > b.timestamp) {
        return -1;
      } else {
        return 0;
      }
    });
  }
);

const MessageFeed = () => {
  const feed = useSelector(getFeed);

  return (
    <div className="message-feed">
      {feed.map((entry) => {
        if (entry.type === 'message') {
          return (
            <MessageFeedEntry
              key={entry.timestamp.toString()}
              {...(entry as ChatMessage)}
            />
          );
        } else {
          return (
            <EventEntry
              key={entry.timestamp.toString()}
              {...(entry as Event)}
            />
          );
        }
      })}
    </div>
  );
};

export default MessageFeed;
