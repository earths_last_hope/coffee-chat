import React, { FunctionComponent, useMemo } from 'react';
import { Event } from '../../reducers/events/types';

const EventEntry: FunctionComponent<Event> = (props) => {
  const { nickname, nicknames, event } = props;

  const eventMessage = useMemo(() => {
    if (event === 'user_online') {
      return `${nickname} has joined`;
    } else if (event === 'user_offline') {
      return `${nickname} has left`;
    } else if (event === 'all_users_online') {
      if (nicknames && nicknames.length > 1) {
        return `${nicknames?.join(', ')} are online now`;
      } else if (nicknames && nicknames.length === 1) {
        return `${nicknames[0]} is online now`;
      } else {
        return 'Nobody else is online as of now';
      }
    } else {
      return '';
    }
  }, [event, nickname, nicknames]);

  return <div className="event-feed-entry">{eventMessage}</div>;
};

export default EventEntry;
