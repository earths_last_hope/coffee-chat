import React, { useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as selectors from '../../reducers/chat/selectors';
import * as actions from '../../reducers/chat/actions';
import { RootReducerState } from '../../reducers';

const Nickname = () => {
  return (
    <div className="nickname">
      <NicknameText />
      <NicknameInput />
    </div>
  );
};

const NicknameText = () => {
  const needsChangeNickname = useSelector((state: RootReducerState) =>
    selectors.getNeedsToChangeNickname(state.chat)
  );

  const nickname = useSelector((state: RootReducerState) =>
    selectors.getNickname(state.chat)
  );

  if (needsChangeNickname) {
    return <span>Choose your nickname</span>;
  } else {
    return (
      <div>
        You are connected as <span className="nickname-text">{nickname}</span>
      </div>
    );
  }
};

const NicknameInput = () => {
  const dispatch = useDispatch();
  const needsChangeNickname = useSelector((state: RootReducerState) =>
    selectors.getNeedsToChangeNickname(state.chat)
  );

  const nickname = useSelector((state: RootReducerState) =>
    selectors.getNickname(state.chat)
  );

  const [nicknameInput, setValue] = useState('');

  const onSubmit = useCallback(() => {
    dispatch(actions.changeNickname(nicknameInput));
    setValue('');
  }, [dispatch, nicknameInput, setValue]);

  if (!needsChangeNickname) {
    return null;
  }

  return (
    <form
      className="change-nickname"
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}
    >
      <input
        type="text"
        value={nicknameInput}
        placeholder={nickname}
        onChange={(e) => setValue(e.target.value)}
      />
      <button>Confirm</button>
    </form>
  );
};

export default Nickname;
