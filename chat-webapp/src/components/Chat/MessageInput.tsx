import React, { useMemo, useCallback, useReducer, useEffect } from 'react';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { createAction, ActionType } from 'typesafe-actions';
import * as actions from '../../reducers/chat/actions';
import * as selectors from '../../reducers/chat/selectors';
import {
  RootReducerState,
  RootReducerAction,
  RootReducerStore,
} from '../../reducers';
import { useKeyPress } from 'use-hooks';

type LocalState = {
  rootStore: RootReducerStore;
  inputText: string;
  historyIndex?: number;
};

const localActions = {
  setInputText: createAction(
    'SET_INPUT_TEXT',
    (inputText: string) => inputText
  )(),
  clearInput: createAction('CLEAR_INPUT')(),
  historyUp: createAction('HISTORY_UP')(),
  historyDown: createAction('HISTORY_DOWN')(),
};

type LocalAction = ActionType<typeof localActions>;

const localReducer: React.Reducer<LocalState, LocalAction> = (
  state,
  action
) => {
  const handleHistoryChange = (change: number) => {
    const rootState = state.rootStore.getState();
    const nextHistoryIndex = (state.historyIndex ?? -1) + change;
    const historyItem = selectors.getHistoryItemForIndex(rootState.chat, {
      index: nextHistoryIndex,
    });

    if (historyItem !== '') {
      return {
        ...state,
        historyIndex: nextHistoryIndex,
        inputText: historyItem,
      };
    } else {
      return state;
    }
  };

  switch (action.type) {
    case 'SET_INPUT_TEXT':
      return {
        ...state,
        inputText: action.payload,
        historyIndex: undefined,
      };
    case 'CLEAR_INPUT': {
      return {
        ...state,
        inputText: '',
        historyIndex: undefined,
      };
    }
    case 'HISTORY_UP': {
      return handleHistoryChange(+1);
    }
    case 'HISTORY_DOWN': {
      if (state.historyIndex === 0) {
        return {
          ...state,
          historyIndex: undefined,
          inputText: '',
        };
      } else {
        return handleHistoryChange(-1);
      }
    }
    default:
      return state;
  }
};

const MessageInput = () => {
  const rootStore = useStore<RootReducerState, RootReducerAction>();
  const [localState, localDispatch] = useReducer(localReducer, {
    rootStore,
    inputText: '',
  });
  const { inputText } = localState;
  const dispatch = useDispatch();

  const needsChangeNickname = useSelector((state: RootReducerState) =>
    selectors.getNeedsToChangeNickname(state.chat)
  );

  const onSubmit = useCallback(() => {
    dispatch(actions.sendChatMessage(inputText));
    localDispatch(localActions.clearInput());
  }, [dispatch, localDispatch, inputText]);

  const isDisabled = useMemo(() => needsChangeNickname, [needsChangeNickname]);

  const upPressed = useKeyPress('ArrowUp', true);
  const downPressed = useKeyPress('ArrowDown', true);

  useEffect(() => {
    if (upPressed) {
      localDispatch(localActions.historyUp());
    }
  }, [upPressed]);

  useEffect(() => {
    if (downPressed) {
      localDispatch(localActions.historyDown());
    }
  }, [downPressed]);

  return (
    <form
      className="message-input"
      onSubmit={(e) => {
        e.preventDefault();
        if (!isDisabled) {
          onSubmit();
        }
      }}
    >
      <input
        readOnly={isDisabled}
        type="text"
        value={inputText}
        onChange={(e) =>
          localDispatch(localActions.setInputText(e.target.value))
        }
      />
      <button type="submit" disabled={isDisabled}>
        Send
      </button>
    </form>
  );
};

export default MessageInput;
