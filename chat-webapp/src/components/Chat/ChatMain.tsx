import React from 'react';
import MessageFeed from './MessageFeed';
import MessageInput from './MessageInput';
import Nickname from './Nickname';
import './styles.scss';

const ChatMain = () => {
  return (
    <div className="chat-main">
      <MessageFeed />
      <Nickname />
      <MessageInput />
    </div>
  );
};

export default ChatMain;
