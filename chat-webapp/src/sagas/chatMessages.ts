import { all, takeEvery, put } from 'redux-saga/effects';
import * as actions from '../reducers/chat/actions';
import * as eventActions from '../reducers/events/actions';
import { ActionType } from 'typesafe-actions';

function* listener() {
  yield all([
    takeEvery('SOCKET_MESSAGE_RECEIVE', receiveSocketMessage),
    takeEvery('SEND_CHAT_MESSAGE', sendChatMessage),
  ]);
}

function* receiveSocketMessage(action: any) {
  const data = JSON.parse(action.message);
  if (data.type === 'message') {
    yield put(actions.receiveChatMessage(data.message, data.sender));
  } else if (data.type === 'event') {
    yield put(eventActions.receiveEvent(data));
  }
}

type SendChatMessageAction = ActionType<typeof actions.sendChatMessage>;

function* sendChatMessage(action: SendChatMessageAction) {
  yield put({
    type: 'SOCKET_MESSAGE_SEND',
    payload: { command: 'message', message: action.payload },
  });
}

export default listener;
