import { takeLeading, select } from 'redux-saga/effects';
import * as actions from '../reducers/chat/actions';
import * as selectors from '../reducers/chat/selectors';
import { ActionType } from 'typesafe-actions';
import { Howl } from 'howler';

function* listen() {
  yield takeLeading('RECEIVE_CHAT_MESSAGE', playMessageIncomingSound);
}

const messageIncomingSound = new Howl({
  src: 'sounds/message_incoming.wav',
});

function* playMessageIncomingSound(
  action: ActionType<typeof actions.receiveChatMessage>
) {
  const nickname = yield select((rootState) =>
    selectors.getNickname(rootState.chat)
  );

  if (action.payload.sender === nickname) {
    return;
  }

  messageIncomingSound.play();
}

export default listen;
