import { all } from 'redux-saga/effects';
import chatMessages from './chatMessages';
import notifications from './notifications';
import changeNickname from './changeNickname';

function* run() {
  yield all([chatMessages(), notifications(), changeNickname()]);
}

export default run;
