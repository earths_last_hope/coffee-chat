import { takeLeading, put } from 'redux-saga/effects';

function* run() {
  yield takeLeading('CHANGE_NICKNAME', changeNicknameSaga);
}

function* changeNicknameSaga(action: any) {
  const nickname = action.payload;

  yield put({
    type: 'SOCKET_MESSAGE_SEND',
    payload: { command: 'nickname', nickname },
  });
}

export default run;
