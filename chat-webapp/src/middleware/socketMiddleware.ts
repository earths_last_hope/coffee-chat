import { MiddlewareAPI } from 'redux';
import url from 'url';

const socketMiddleware = (store: MiddlewareAPI<any, any>) => {
  const sock = new WebSocket(
    url.format({
      protocol: window.location.protocol === 'https:' ? 'wss' : 'ws',
      hostname: process.env.CHAT_WS_HOST || window.location.hostname,
      port: process.env.CHAT_WS_PORT || 8888,
    })
  );

  sock.onmessage = (event) => {
    store.dispatch({ type: 'SOCKET_MESSAGE_RECEIVE', message: event.data });
  };

  sock.onopen = (event) => {
    store.dispatch({ type: 'SOCKET_OPEN' });
  };

  sock.onclose = (event) => {
    store.dispatch({ type: 'SOCKET_CLOSE' });
  };

  sock.onerror = (event) => {
    store.dispatch({ type: 'SOCKET_ERROR' });
  };

  return (next: (action: any) => void) => (action: any) => {
    if (
      action.type &&
      action.type === 'SOCKET_MESSAGE_SEND' &&
      sock.readyState === 1
    ) {
      sock.send(JSON.stringify(action.payload));
    }

    return next(action);
  };
};

export default socketMiddleware;
