import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from './reducers';
import { socketMiddleware } from './middleware';
import mainSaga from './sagas';

const anyWindow = window as any;

const sagaMiddleware = createSagaMiddleware();

let composedEnhancers = compose(
  applyMiddleware(socketMiddleware, sagaMiddleware)
);

if (
  process.env.NODE_ENV === 'development' &&
  anyWindow.__REDUX_DEVTOOLS_EXTENSION__
) {
  composedEnhancers = compose(
    composedEnhancers,
    anyWindow.__REDUX_DEVTOOLS_EXTENSION__()
  );
}

const store = createStore(rootReducer, composedEnhancers);

sagaMiddleware.run(mainSaga);

export default store;
