import { createAction } from 'typesafe-actions';
import { connectionOpen, connectionClose } from '../ws/actions';

const receiveNickname = createAction(
  'RECEIVE_NICKNAME',
  (connectionId: string, nickname: string) => ({
    connectionId,
    nickname
  })
)();

export { connectionOpen, receiveNickname, connectionClose };
