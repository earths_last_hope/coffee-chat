import { createReducer } from 'typesafe-actions';
import { ConnectionState, ConnectionAction } from './types';
import * as actions from './actions';

const reducer = createReducer<ConnectionState, ConnectionAction>({})
  .handleAction(actions.connectionOpen, (state, action) => {
    const { connectionId, ipAddress } = action.payload;
    return {
      ...state,
      [connectionId]: {
        ipAddress,
        connectedAt: Date.now()
      }
    };
  })
  .handleAction(actions.connectionClose, (state, action) => {
    const connectionId = action.payload;
    return {
      ...state,
      [connectionId]: {
        ...state[connectionId],
        disconnectedAt: Date.now()
      }
    };
  })
  .handleAction(actions.receiveNickname, (state, action) => {
    const { connectionId, nickname } = action.payload;
    return {
      ...state,
      [connectionId]: {
        ...state[connectionId],
        nickname
      }
    };
  });

export default reducer;
