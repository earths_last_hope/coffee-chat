import { createSelector } from 'reselect';
import { ConnectionState, Connection } from './types';
import { RootState } from '../rootReducer';

type ConnectionArrayRecord = { connectionId: string } & Connection;

const getConnectionState = (state: RootState) => state.connections;

const getAllConnection = createSelector<
  RootState,
  ConnectionState,
  ConnectionArrayRecord[]
>([getConnectionState], (connectionState) => {
  const keys = Object.keys(connectionState);

  return keys.map((connectionId) => ({
    connectionId,
    ...connectionState[connectionId]
  }));
});

const getConnectionById = (
  state: RootState,
  props: { connectionId: string }
) => {
  return state.connections[props.connectionId];
};

const getOpenConnections = createSelector([getAllConnection], (connections) =>
  connections.filter(
    (x) => x.disconnectedAt === undefined && x.nickname !== undefined
  )
);

export { getConnectionById, getOpenConnections };
