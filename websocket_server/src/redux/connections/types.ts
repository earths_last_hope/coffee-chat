import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type Connection = {
  nickname?: string;
  connectedAt: number;
  disconnectedAt?: number;
  ipAddress: string;
};

export type ConnectionState = { [connectionId: string]: Connection };
export type ConnectionAction = ActionType<typeof actions>;
