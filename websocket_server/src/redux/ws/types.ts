export type WsPayload = {
  command: 'message' | 'nickname';
  message?: string;
  nickname?: string;
};
