import { createAction } from 'typesafe-actions';

const connectionOpen = createAction(
  'WS_CONNECTION_OPEN',
  (connectionId: string, ipAddress: string) => ({ connectionId, ipAddress })
)();

const connectionClose = createAction(
  'WS_CONNECTION_CLOSE',
  (connectionId: string) => connectionId
)();

const messageSend = createAction(
  'WS_MESSAGE_SEND',
  (connectionId: string, message: string) => ({ connectionId, message })
)();

const messageReceive = createAction(
  'WS_MESSAGE_RECEIVE',
  (connectionId: string, message: string) => ({ connectionId, message })
)();

const error = createAction(
  'WS_ERROR',
  (connectionId: string, error: Error) => ({ connectionId, error })
)();

export { connectionOpen, connectionClose, messageSend, messageReceive, error };
