import { Server } from 'ws';
import { Middleware } from 'redux';
import { isActionOf } from 'typesafe-actions';
import * as actions from './actions';
import newConnectionId from '../../lib/newConnectionId';

const createWebsocketMiddleware = (websocketServer: Server): Middleware => (
  store
) => {
  let instances: { [connectionId: string]: any } = {};

  websocketServer.on('connection', function connection(ws, req) {
    const connectionId = newConnectionId();
    instances[connectionId] = ws;

    store.dispatch(
      actions.connectionOpen(connectionId, req.connection.remoteAddress)
    );

    ws.on('close', (code, reason) => {
      store.dispatch(actions.connectionClose(connectionId));
    });

    ws.on('error', (error) => {
      store.dispatch(actions.error(connectionId, error));
    });

    ws.on('message', (data) => {
      store.dispatch(actions.messageReceive(connectionId, data.toString()));
    });
  });

  return (next) => (action) => {
    if (isActionOf(actions.messageSend, action)) {
      const instance = instances[action.payload.connectionId];
      instance.send(action.payload.message);
    }

    return next(action);
  };
};

export default createWebsocketMiddleware;
