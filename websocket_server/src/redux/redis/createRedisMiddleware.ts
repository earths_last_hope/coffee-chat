import { createClient } from 'redis';
import { Middleware } from 'redux';
import { isActionOf } from 'typesafe-actions';
import * as actions from './actions';
import { RedisPayload } from './types';

const createRedisMiddleware = (
  redisServer: string,
  channelName: string
): Middleware => (store) => {
  const redisSub = createClient(redisServer);
  redisSub.subscribe(channelName);

  redisSub.on('message', (channel, message) => {
    store.dispatch(actions.message(message, channel));
  });

  const redisPub = createClient(redisServer);

  return (next) => (action) => {
    if (isActionOf(actions.publish, action)) {
      const payload: RedisPayload = {
        message: action.payload.message,
        senderId: action.payload.senderId
      };
      redisPub.publish(
        action.payload.channel ?? channelName,
        JSON.stringify(payload)
      );
    }

    return next(action);
  };
};

export default createRedisMiddleware;
