import { createAction } from 'typesafe-actions';

const message = createAction(
  'REDIS_MESSAGE',
  (message: string, channel?: string) => ({
    channel,
    message
  })
)();

const publish = createAction(
  'REDIS_PUBLISH',
  (message: string, senderId: string, channel?: string) => ({
    channel,
    message,
    senderId
  })
)();

export { message, publish };
