export type RedisPayload = {
  message: string;
  senderId: string;
};
