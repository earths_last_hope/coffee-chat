import { all } from 'redux-saga/effects';
import chatRelaySaga from './chatRelaySaga';

function* run() {
  yield all([chatRelaySaga()]);
}

export default run;
