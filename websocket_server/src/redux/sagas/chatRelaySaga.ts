import { all, takeEvery, put, select, take } from 'redux-saga/effects';
import { getType, ActionType } from 'typesafe-actions';
import * as wsActions from '../ws/actions';
import * as redisActions from '../redis/actions';
import * as connectionActions from '../connections/actions';
import * as connectionSelectors from '../connections/selectors';
import { RedisPayload } from '../redis/types';
import { WsPayload } from '../ws/types';

function* chatRelaySaga() {
  yield all([
    takeEvery(getType(wsActions.messageReceive), wsMessageReceived),
    takeEvery(getType(redisActions.message), redisMessageReceived),
    takeEvery(getType(wsActions.connectionOpen), notifyUserOnline),
    takeEvery(getType(wsActions.connectionClose), notifyUserOffline)
  ]);
}

function* wsMessageReceived(
  action: ActionType<typeof wsActions.messageReceive>
) {
  const { connectionId, message } = action.payload;
  const wsPayload: WsPayload = JSON.parse(message);

  if (wsPayload.command === 'message') {
    yield put(redisActions.publish(wsPayload.message, connectionId));
  } else if (wsPayload.command === 'nickname') {
    yield put(
      connectionActions.receiveNickname(connectionId, wsPayload.nickname)
    );
  }
}

function* redisMessageReceived(
  action: ActionType<typeof redisActions.message>
) {
  const redisPayload: RedisPayload = JSON.parse(action.payload.message);
  const recipients = yield select(connectionSelectors.getOpenConnections);
  const sender = yield select((state) =>
    connectionSelectors.getConnectionById(state, {
      connectionId: redisPayload.senderId
    })
  );
  const wsPayload = {
    type: 'message',
    message: redisPayload.message,
    sender: sender.nickname
  };
  yield all(
    recipients.map((r) =>
      put(wsActions.messageSend(r.connectionId, JSON.stringify(wsPayload)))
    )
  );
}

type ReceiveNicknameAction = ActionType<
  typeof connectionActions.receiveNickname
>;

function* notifyUserOnline(
  action: ActionType<typeof wsActions.connectionOpen>
) {
  const { connectionId } = action.payload;

  // Wait for nickname to be set
  const receiveNicknameAction: ReceiveNicknameAction = yield take(
    (action: ReceiveNicknameAction) =>
      action.type === 'RECEIVE_NICKNAME' &&
      action.payload.connectionId === connectionId
  );

  const {
    payload: { nickname }
  } = receiveNicknameAction;

  const openConnections = yield select(connectionSelectors.getOpenConnections);

  const connectedUsers = openConnections.filter(
    (x) => x.connectionId !== connectionId
  );

  const wsPayload = {
    type: 'event',
    event: 'user_online',
    nickname
  };

  yield all(
    connectedUsers.map((r) =>
      put(wsActions.messageSend(r.connectionId, JSON.stringify(wsPayload)))
    )
  );

  yield put(
    wsActions.messageSend(
      connectionId,
      JSON.stringify({
        type: 'event',
        event: 'all_users_online',
        nicknames: connectedUsers.map((r) => r.nickname)
      })
    )
  );
}

function* notifyUserOffline(
  action: ActionType<typeof wsActions.connectionClose>
) {
  const { payload: connectionId } = action;

  const userData = yield select((state) =>
    connectionSelectors.getConnectionById(state, {
      connectionId
    })
  );

  const openConnections = yield select(connectionSelectors.getOpenConnections);

  const recipients = openConnections.filter(
    (x) => x.connectionId !== connectionId
  );

  const wsPayload = {
    type: 'event',
    event: 'user_offline',
    nickname: userData.nickname
  };

  yield all(
    recipients.map((r) =>
      put(wsActions.messageSend(r.connectionId, JSON.stringify(wsPayload)))
    )
  );
}

export default chatRelaySaga;
