import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import createRedisMiddleware from './redis/createRedisMiddleware';
import createWebsocketMiddleware from './ws/createWebsocketMiddleware';
import rootReducer from './rootReducer';
import { Server as WebsocketServer } from 'ws';
import createSagaMiddleware from 'redux-saga';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

const createAppStore = (
  server: WebsocketServer,
  redisServer: string,
  redisChannel: string
) => {
  const store = createStore(
    rootReducer,
    applyMiddleware(
      createRedisMiddleware(redisServer, redisChannel),
      createWebsocketMiddleware(server),
      sagaMiddleware,
      logger
    )
  );
  sagaMiddleware.run(sagas);

  return store;
};

export default createAppStore;
