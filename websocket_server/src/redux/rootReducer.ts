import { combineReducers } from 'redux';
import connections from './connections/reducer';
import { StateType } from 'typesafe-actions';

const rootReducer = combineReducers({ connections });

export type RootState = StateType<typeof rootReducer>;

export default rootReducer;
