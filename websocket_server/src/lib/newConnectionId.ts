import * as cryptoRandomString from 'crypto-random-string';

const newConnectionId = () =>
  cryptoRandomString({
    length: 10
  });

export default newConnectionId;
