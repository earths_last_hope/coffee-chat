import { Server as WebSocketServer } from 'ws';
import createStore from './redux/store';

const WEBSOCKET_PORT = parseInt(process.env.WEBSOCKET_PORT || '8888');
const REDIS_SERVER = process.env.REDIS_SERVER || 'redis://localhost:6379';
const REDIS_CHANNEL = 'chat';

const server = new WebSocketServer({ port: WEBSOCKET_PORT });

console.log('WebSocket server started at ws://locahost:' + WEBSOCKET_PORT);

const store = createStore(server, REDIS_SERVER, REDIS_CHANNEL);
